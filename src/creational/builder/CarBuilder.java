package creational.builder;

/**
 * Created by zwspdak on 2016-04-27.
 */
public class CarBuilder {

    private String name;
    private String type;
    private int maxSpeed;
    private String fuel;
    private int power;


    public int getPower() {
        return power;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public String getFuel() {
        return fuel;
    }

    public CarBuilder name(String name) {
        this.name = name;
        return this;
    }

    public CarBuilder type(String type) {
        this.type = type;
        return this;
    }

    public CarBuilder maxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
        return this;
    }

    public CarBuilder fuel(String fuel) {
        this.fuel = fuel;
        return this;
    }

    public CarBuilder power(int power) {
        this.power = power;
        return this;
    }

    public Car build() {
        return new Car(this);
    }
}
