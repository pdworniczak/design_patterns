package creational.builder;

/**
 * Created by zwspdak on 2016-04-27.
 */
public class Car {

    private String name;
    private String type;
    private int maxSpeed;
    private String fuel;
    private int power;

    public Car(CarBuilder builder) {
        name = builder.getName();
        type = builder.getType();
        maxSpeed = builder.getMaxSpeed();
        fuel = builder.getFuel();
        power = builder.getPower();
    }

    @Override
    public String toString() {
        return new StringBuilder().append("name: ").append(name).append("\n").append("type: ").append(type).append("\n").
                append("fuel: ").append(fuel).append("\n").append("power: ").append(power).append("\n").append("maxSpeed: ").
                append(maxSpeed).append("\n").toString();
    }
}
