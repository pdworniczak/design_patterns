package creational.prototype;

/**
 * Created by zwspdak on 2016-04-27.
 */
public class Monkey extends Animal {
    @Override
    public void sound() {
        System.out.println("Uua a a a a a");
    }
}
