package creational.prototype;

/**
 * Created by zwspdak on 2016-04-27.
 */
public abstract class Animal implements Cloneable {

    public Animal clone() {
        try {
            return (Animal) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public abstract void sound();
}
