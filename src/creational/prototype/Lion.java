package creational.prototype;

/**
 * Created by zwspdak on 2016-04-27.
 */
public class Lion extends Animal {
    @Override
    public void sound() {
        System.out.println("Roar!!!");
    }
}
