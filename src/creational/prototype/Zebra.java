package creational.prototype;

/**
 * Created by zwspdak on 2016-04-27.
 */
public class Zebra extends Animal {
    @Override
    public void sound() {
        System.out.println("Ioioioio");
    }
}
