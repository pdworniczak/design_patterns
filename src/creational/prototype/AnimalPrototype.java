package creational.prototype;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zwspdak on 2016-04-27.
 */
public class AnimalPrototype {

    public static final String ZEBRA = "zebra";
    public static final String LION = "lion";
    public static final String MONKEY = "monkey";

    private static Map<String, Animal> animals = new HashMap<>();

    static {
        animals.put(ZEBRA, new Zebra());
        animals.put(LION, new Lion());
        animals.put(MONKEY, new Monkey());
    }


    public static Animal getAnimal(String animalName) throws Exception {
        switch(animalName) {
            case ZEBRA: return animals.get(ZEBRA).clone();
            case LION: return animals.get(LION).clone();
            case MONKEY: return animals.get(MONKEY).clone();
        }
        throw new Exception();
    }
}
