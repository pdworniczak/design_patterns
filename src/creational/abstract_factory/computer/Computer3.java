package creational.abstract_factory.computer;

import creational.abstract_factory.disc.DiscFactory;
import creational.abstract_factory.disc.HardDisc;
import creational.abstract_factory.processor.Processor;
import creational.abstract_factory.processor.ProcessorFactory;

/**
 * Created by zwspdak on 2016-04-27.
 */
public class Computer3 implements Computer {
    private HardDisc hd;
    private Processor proc;

    public Computer3() {
        hd = DiscFactory.getDisc("hdd");
        proc = ProcessorFactory.getProcessor("amd");
    }

    @Override
    public String toString() {
        return new StringBuilder().append("processr: ").append(proc).append("\n").append("disc: ").append(hd).append("\n").toString();
    }
}
