package creational.abstract_factory;

import creational.abstract_factory.computer.Computer;
import creational.abstract_factory.computer.Computer1;
import creational.abstract_factory.computer.Computer2;
import creational.abstract_factory.computer.Computer3;

/**
 * Created by zwspdak on 2016-04-27.
 */
public class ComputerFactory {

    public static Computer createComputer(int type) {
        Computer computer = null;
        switch (type) {
            case 1: computer = new Computer1();
                break;
            case 2: computer = new Computer2();
                break;
            case 3: computer = new Computer3();
        }
        return computer;
    };
}
