package creational.abstract_factory.processor;

/**
 * Created by zwspdak on 2016-04-27.
 */
public class AMDProcessor implements Processor {

    @Override
    public String toString() {
        return "AMD processor";
    }
}
