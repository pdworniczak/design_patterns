package creational.abstract_factory.processor;

/**
 * Created by zwspdak on 2016-04-27.
 */
public class ProcessorFactory {

    public static Processor getProcessor(String name) {
        switch (name) {
            case "intel": return new IntelProcessor();
            case "amd": return new AMDProcessor();
        }
        return null;
    }
}
