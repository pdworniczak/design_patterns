package creational.abstract_factory.processor;

/**
 * Created by zwspdak on 2016-04-27.
 */
public class IntelProcessor implements Processor {

    @Override
    public String toString() {
        return "intel processor";
    }
}
