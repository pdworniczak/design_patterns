package creational.abstract_factory.disc;

/**
 * Created by zwspdak on 2016-04-27.
 */
public class DiscFactory {

    public static HardDisc getDisc(String type) {
        switch(type) {
            case "sdd": return new SddDisc();
            case "hdd": return new Hddisc();
        }
        return null;
    }
}
