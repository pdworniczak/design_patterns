package creational.abstract_factory.disc;

/**
 * Created by zwspdak on 2016-04-27.
 */
public class SddDisc implements HardDisc {

    @Override
    public String toString() {
        return "sdd disc";
    }
}
