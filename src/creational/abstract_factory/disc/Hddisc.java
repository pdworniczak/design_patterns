package creational.abstract_factory.disc;

import creational.abstract_factory.disc.HardDisc;

/**
 * Created by zwspdak on 2016-04-27.
 */
public class Hddisc implements HardDisc {

    @Override
    public String toString() {
        return "hdd disc";
    }
}
