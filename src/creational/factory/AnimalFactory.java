package creational.factory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zwspdak on 2016-04-27.
 */
public class AnimalFactory {

    public static final String ZEBRA = "zebra";
    public static final String LION = "lion";
    public static final String MONKEY = "monkey";


    public static Animal getAnimal(String animalName) throws Exception {
        switch(animalName) {
            case ZEBRA: return new Zebra();
            case LION: return new Lion();
            case MONKEY: return new Monkey();
        }
        throw new Exception();
    }
}
