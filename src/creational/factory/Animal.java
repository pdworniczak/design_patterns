package creational.factory;

/**
 * Created by zwspdak on 2016-04-27.
 */
public interface Animal {

    void sound();
}
