package creational.factory;

/**
 * Created by zwspdak on 2016-04-27.
 */
public class Zebra implements Animal {
    @Override
    public void sound() {
        System.out.println("Ioioioio");
    }
}
