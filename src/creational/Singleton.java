package creational;

/**
 * Created by zwspdak on 2016-04-27.
 */
public class Singleton {

    private static Singleton instance;
    private static int count = 0;

    private Singleton(){};

    public synchronized static Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton();
            count += 1;
        }

        return instance;
    }

    @Override
    public String toString() {
        return count + " singleton";
    }
}
