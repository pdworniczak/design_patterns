import creational.Singleton;
import creational.abstract_factory.ComputerFactory;
import creational.abstract_factory.computer.Computer;
import creational.builder.Car;
import creational.builder.CarBuilder;
import creational.factory.AnimalFactory;
import creational.prototype.AnimalPrototype;
import creational.prototype.Animal;

/**
 * Created by zwspdak on 2016-04-27.
 */
public class DesignPatterns {

    public static void main(String[] args) throws Exception {

        testSingleton();
        testPrototype();
        testFactory();
        testBuilder();
        testAbstractFactory();

    }

    public static void testSingleton() {
        System.out.println("===TEST Singleton===");
        Singleton singleton = Singleton.getInstance();
        System.out.println(singleton);
        Singleton newSingleton = Singleton.getInstance();
        System.out.println(newSingleton);
        System.out.println("singleton and newSingleton is sanme instance?: " + (singleton == newSingleton));
        System.out.println("===END TEST===");
    }

    public static void testPrototype() throws Exception {
        System.out.println("===TEST Prototype===");
        Animal lion1 = AnimalPrototype.getAnimal(AnimalPrototype.LION);
        Animal lion2 = AnimalPrototype.getAnimal(AnimalPrototype.LION);
        Animal zebra1 = AnimalPrototype.getAnimal(AnimalPrototype.ZEBRA);
        Animal zebra2 = AnimalPrototype.getAnimal(AnimalPrototype.ZEBRA);
        Animal zebra3 = AnimalPrototype.getAnimal(AnimalPrototype.ZEBRA);
        Animal monkey1 = AnimalPrototype.getAnimal(AnimalPrototype.MONKEY);
        Animal monkey2 = AnimalPrototype.getAnimal(AnimalPrototype.MONKEY);
        lion1.sound();
        zebra2.sound();
        monkey1.sound();
        System.out.println();
        System.out.println(lion1 == lion2);
        System.out.println((zebra1 == zebra2) + " " + (zebra2 == zebra3) + " " + (zebra1 == zebra3));
        System.out.println("===END TEST===");
    }

    public static void testFactory() throws Exception {
        System.out.println("===TEST Factory===");
        creational.factory.Animal lion1 = AnimalFactory.getAnimal(AnimalFactory.LION);
        creational.factory.Animal lion2 = AnimalFactory.getAnimal(AnimalFactory.LION);
        creational.factory.Animal zebra1 = AnimalFactory.getAnimal(AnimalFactory.ZEBRA);
        creational.factory.Animal zebra2 = AnimalFactory.getAnimal(AnimalFactory.ZEBRA);
        creational.factory.Animal zebra3 = AnimalFactory.getAnimal(AnimalFactory.ZEBRA);
        creational.factory.Animal monkey1 = AnimalFactory.getAnimal(AnimalFactory.MONKEY);
        creational.factory.Animal monkey2 = AnimalFactory.getAnimal(AnimalFactory.MONKEY);
        lion1.sound();
        zebra2.sound();
        monkey1.sound();
        System.out.println();
        System.out.println(lion1 == lion2);
        System.out.println((zebra1 == zebra2) + " " + (zebra2 == zebra3) + " " + (zebra1 == zebra3));
        System.out.println("===END TEST===");
    }

    public static void testBuilder() {
        System.out.println("===TEST Builder===");
        Car car1 = new CarBuilder()
                .type("sport")
                .fuel("benzyna")
                .name("ferrari")
                .maxSpeed(320)
                .power(250)
                .build();
        Car car2 = new CarBuilder()
                .type("osobowy")
                .fuel("olej")
                .name("audi a3")
                .maxSpeed(220)
                .power(197)
                .build();
        System.out.println(car1 + "\n" + car2);
        System.out.println("===END TEST===");
    }

    public static void testAbstractFactory() {
        System.out.println("===TEST Abstract Factory===");
        Computer comp1 = ComputerFactory.createComputer(1);
        Computer comp2 = ComputerFactory.createComputer(2);
        Computer comp3 = ComputerFactory.createComputer(3);
        System.out.println(comp1 + "\n" + comp2 + "\n" + comp3);
        System.out.println("===END TEST===");
    }
}
